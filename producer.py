import pika
import sys
import random
import time

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('127.0.0.1', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()
channel.exchange_declare(exchange = 'logs', exchange_type = 'funout')

while True:

	i = random.randint(1, 100)
	
	channel.basic_publish(
		exchange = 'logs',
		routing_key = '',
		body = i,
		properties = pika.BasicProperties(
			delivery_mode = pika.spec.PERSISTENT_DELIVERY_MODE
	))	

connection.close()


	#if i % 2 == 0
	#	key = 'even'
	#else:
	#	key = 'odd'
	#p.publish(str(i), key)
	#print 'integer: %d' % i
	#time.sleep(1)

#connection.close()

#channel.queue_declare(queue = 'task_queue', durable = True)

#message = ' '.join(sys.argv[1:]) or "Hello World!"

#channel.basic_publish(
#	exchange = '',
#	routing_key = 'task_queue',
#	body = message,
#	properties = pika.BasicProperties(
#		delivery_mode = 2,
#	))
#print(" [x] Sent %r" % message)
#connection.close()