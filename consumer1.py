import pika
import sys
import random
import time

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('127.0.0.1', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()

#channel.queue_declare(queue_name = 'odds', routing_key = odd)

def callBack(ch, method, properties, body):

	i = body
	if i % 2 != 0
		print(body)
	ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count = 1)
channel.basic_consume(queue = 'task_queue', on_message_callback = callBack)

channel.wait()

